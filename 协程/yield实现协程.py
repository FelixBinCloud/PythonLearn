# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-13 15:19'


def consumer(name):
    print("--->starting eating baozi...")
    while True:
        new_baozi = yield
        print("[%s] is eating baozi %s" % (name, new_baozi))


def producer():
    r = con.__next__()
    n = 0
    while n < 5:
        n += 1
        con.send(n)  # （1）唤醒生成器 （2）"sends" a value
        print("\033[32;1m[producer]\033[0m is making baozi %s" % n)


if __name__ == '__main__':
    con = consumer("c1")
    p = producer()