# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-13 16:59'
'''
gevent:遇到IO操作就自动切换

'''

import gevent

def foo():
    print('run in foo')
    gevent.sleep(2) # 模拟io操作
    print('explicit(精确的) context switch to foo again')


def bar():
    print('Explicit context to bar')
    gevent.sleep(1)
    print('Implicit context switch back to bar')


gevent.joinall([
    gevent.spawn(foo),  # 启动协程
    gevent.spawn(bar)
])