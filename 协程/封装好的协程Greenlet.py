# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-13 16:48'

from greenlet import greenlet


def test1():
    print(12)
    gr2.switch()  # 切换
    print(34)
    gr2.switch()


def test2():
    print(56)
    gr1.switch()
    print(78)


gr1 = greenlet(test1)  # 启动一个协程
gr2 = greenlet(test2)
gr1.switch()


'''
greenlet手动切换，gevent自动切换。在gevent中封装了greenlet

'''