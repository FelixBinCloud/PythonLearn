# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-13 10:31'


''''
不同进程间内存是不共享的，要想实现两个进程间的数据交换需要中间翻译者可以使用的方法有Queues、Pipes、Managers
'''

# 进程Queues
from multiprocessing import Process, Queue


def f(q):
    q.put([[42, None, 'hello']])


if __name__ == "__main__":  # 主进程下
    q = Queue()
    p = Process(target=f,args=(q,))
    p.start()
    print(q.get())
    p.join()

'''
（0）进程间是独立的内存空间
（1）当前程序是主进程，p = Process(target=f,args=(q,))是开启一个进程去处理函数f是一个子进程，主进程与子进程使用数据传递使用进程Queues。
（2）Queue只是实现进程间数据的传递
（3）Join()是主程序等我这个进程执行完毕了，程序才往下走
 (4)实质为：克隆。 有一个中间翻译，可以序列化以及反序列化从而实现这两个克隆对象间的数据传递。

'''