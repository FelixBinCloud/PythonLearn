# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-13 11:00'


'''
不同进程间内存是不共享的，要想实现两个进程间的数据交换需要中间翻译者可以使用的方法有Queues、Pipes、Managers
'''

from multiprocessing import Process, Manager
import os


def f(d, l):
    d[1] = '1'
    d['2'] = 2
    d["pid%s" % os.getpid()] = os.getpid()
    l.append(1)
    print(l, d)


if __name__ == '__main__':
    with Manager() as manager:
        d = manager.dict()
        l = manager.list(range(5))

        p_list = []
        for i in range(10):
            p = Process(target=f, args=(d, l))
            p.start()
            p_list.append(p)
        for res in p_list:
            res.join()
        l.append("from parent")
        print(d)
        print(l)



''''
with如何工作？
紧跟with后面的语句被求值后，返回对象的__enter__()方法被调用，这个方法的返回值将被赋值给as后面的变量。当with后面的代码块全部被执行完之后，将调用前面返回对象的__exit__()方法。

Manager（）返回的管理器对象 控制一个服务器进程，该进程保存Python对象并允许其他进程使用代理操作它们。

实质为：默认已加锁，克隆多个出去给每个进程操作，然后再把他们合在一起，最终实现数据的共享
'''