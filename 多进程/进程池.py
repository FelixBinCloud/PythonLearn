# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-13 11:34'


from multiprocessing import Process, Pool, freeze_support
import time
import os


def Foo(i):
    time.sleep(2)
    print("in process", os.getpid())
    return i + 100


def Bar(arg):
    print('-->exec done:', arg, os.getpid())


if __name__ == '__main__':
    #freeze_support()
    pool = Pool(processes=3)  # 允许进程池同时放入3个进程
    print("主进程", os.getpid())
    for i in range(10):
        pool.apply_async(func=Foo, args=(i,), callback=Bar)  # callback=回调

        #pool.apply(func=Foo, args=(i,))  # 串行
        #pool.apply_async(func=Foo, args=(i,))  # 串行

    print('end')
    pool.close()
    pool.join()  # 进程池中进程执行完毕后再关闭，如果注释，那么程序直接关闭。.join()

'''
pool.close()
pool.join()
close与join的顺序不能调换

执行此函数（主进程）与调用callback回调函数的进程（子进程）是同一个进程，也就是主进程调用回调函数。
'''