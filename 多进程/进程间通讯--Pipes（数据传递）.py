# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-13 10:44'

''''
不同进程间内存是不共享的，要想实现两个进程间的数据交换需要中间翻译者可以使用的方法有Queues、Pipes、Managers
'''

# 管道Pipes  有两个头：父头 --子头

from multiprocessing import Process, Pipe


def f(conn):
    conn.send([42, None, 'hello from child'])  # 向父亲发送信息
    conn.close()


if __name__ == "__main__":
    parent_conn, child_conn = Pipe()
    p = Process(target=f, args=(child_conn,))
    p.start()
    print(parent_conn.recv())  # 接受消息
    p.join()


'''
Pipe（）返回的两个连接对象代表管道的两端。每个连接对象都有send（）和recv（）方法（以及其他方法）。请注意，如果两个进程（或线程）同时尝试读取或写入管道的同一端，则管道中的数据可能会损坏。当然，同时使用管道的不同端部的过程不存在损坏的风险。
'''