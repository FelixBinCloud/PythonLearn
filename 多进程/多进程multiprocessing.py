# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-13 9:44'


from multiprocessing import Process

import time, os

#
# def f(name):
#     time.sleep(2)
#     print("hello", name)
#
# if __name__ == "__main__":
#     p = Process(target=f, args=('bob',))
#     p.start()
#     p.join()
'''
在父进程了与子进程里调用同一个方法查看进程变化：
(1)第一个info 父进程id是开发工具的进程号
(2)第二个info的父进程id是当前程序的进程id

'''


def info(title):
    print(title)
    print("module name:", __name__)
    print("parent process:", os.getppid())  # 父ID
    print("process id", os.getpid())
    print("\n\n")


def f1(name):
    info('\033[31;1mfunction f1\033[0m')
    print('hello', name)


if __name__ == "__main__":
    info('\033[32;1mmain process line\033[0m')
    p = Process(target=f1, args=("bob",))
    p.start()
    p.join()

'''
没有启多线程时默认有一个，开发工具的进程号
每一个进程都是有父进程启动的
上述id==》开发工具进程id----》当前程序进程id
'''