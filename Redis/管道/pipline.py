# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-20 16:51'

import redis
import time
pool = redis.ConnectionPool(host='127.0.0.1', port=6379,db=4)

r = redis.Redis(connection_pool=pool)

# pipe = r.pipeline(transaction=False)
pipe = r.pipeline(transaction=True)
pipe.multi()
pipe.set('name', 'test')
time.sleep(50)
pipe.set('role', 'sb')

pipe.execute()