# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-20 17:28'


import redis


class RedisHelper:
    '''初始化时
    （1）连接指定服务器
    （2）定义发布消息的频道
    （3）定义订阅消息的频道
    '''
    def __init__(self):
        self.__conn = redis.Redis(host='127.0.0.1')
        self.chan_sub = 'fm104.5'
        self.chan_pub = 'fm104.5'


 # 发布消息
    def public(self, msg):
        self.__conn.publish(self.chan_pub, msg) # 调用redis的publish方法
        return True


# 订阅消息
    def subscribe(self):
        pub = self.__conn.pubsub()  # 打开收音机
        pub.subscribe(self.chan_sub)  # 调频道
        pub.parse_response()  # 准备接受
        return pub