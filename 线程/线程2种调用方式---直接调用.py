# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-11 8:59'

import threading
import time


# 直接调用：
def run(num):  # 定义每个线程要运行的函数
    print("running on number:%s" % num)
    time.sleep(3)

if __name__ == '__main__':
    t1 = threading.Thread(target=run, args=(1,))  # 生成一个线程实例
    t2 = threading.Thread(target=run, args=(2,))  # 生成另一个线程实例

    t1.start()  # 启动线程
    t2.start()  # 启动另一个线程

    print(t1.getName())  # 获取线程名


