# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-11 9:29'


import time
import threading


def run(n):
    print('[%s]------running----\n' % n)
    time.sleep(2)
    print('--done--')


def main():
    for i in range(5):
        t = threading.Thread(target=run, args=[i, ])
        t.start()
        t.join(1)
        print('starting thread', t.getName())


m = threading.Thread(target=main, args=[])
m.setDaemon(True)  # 将main线程设置为Daemon线程,它做为程序主线程的守护线程,当主线程退出时,m线程也会退出,由m启动的其它子线程会同时退出,不管是否执行完任务
m.start()
m.join(timeout=1)
print("---main thread done----")


'''
（1）Thread对象的join方法：如果线程或者函数在执行过程中调用了另一个线程，需要等待当前调用的线程完成后
在继续当前线程或者函数的执行。简而言之就是某个线程使用join方法需要等待当前线程结果返回，才能去执行其他的线程。
（2）setDaemon（True）:将指定的线程变为守护线程， 必须在start() 方法调用之前设置，如果不设置为守护线程，程序会被无限挂起。效果为只要主线程完成了，不管子线程（守护线程）是否完成，都要和主线程一起退出。
=====>所以，join和setDaemon的区别如上述的例子，它们基本是相反的。
'''