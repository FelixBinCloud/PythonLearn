# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-11 15:37'


import threading, time
import random


def light():
    if not event.isSet():
        event.set() #wait就不阻塞 #绿灯状态
    count = 0
    while True:
        if count < 10:
            print('\033[42;1m--green light on---\033[0m')
        elif count <13:
            print('\033[43;1m--yellow light on---\033[0m')
        elif count <20:
            if event.isSet():
                event.clear()
            print('\033[41;1m--red light on---\033[0m')
        else:
            count = 0
            event.set() #打开绿灯
        time.sleep(1)
        count += 1
def car(n):
    while 1:
        time.sleep(random.randrange(10))
        if  event.isSet(): #绿灯
            print("car [%s] is running.." % n)
        else:
            print("car [%s] is waiting for the red light.." %n)
if __name__ == '__main__':
    event = threading.Event()
    Light = threading.Thread(target=light)
    Light.start()
    for i in range(3):
        t = threading.Thread(target=car,args=(i,))
        t.start()

'''
Event四个方法：
（1） event.isSet()  ：判断标志位是否设定
（2）event.wait()    ：等待标志位被设定
（3）event.set()     ：设定标志位 变为1
（4）event.clear()   ：清空标志位 变为0
'''