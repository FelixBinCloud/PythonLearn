# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-11 9:08'


import threading
import time


class MyThread(threading.Thread): # 继承threading.Thread
    def __init__(self, num):
        threading.Thread.__init__(self)  # 重构父类
        self.num = num

    def run(self):  # 定义每个线程要运行的函数   此时必须是run方法

        print("running on number:%s" % self.num)

        time.sleep(3)


if __name__ == '__main__':
    t1 = MyThread(1)
    t2 = MyThread(2)

    t1.start()
    t2.start()