# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-11 17:39'


import threading
import queue
import time

q = queue.Queue()

# 生产者
def producer(name):
    count = 1
    while True:
        q.put(count)
        print("[%s]生产了【%s】个骨头"%(name,count))
        count += 1
        time.sleep(1)

# 消费者
def consumer(name):
    while True:
        print("[%s]取到了骨头【%s】" % (name, q.get()))

p = threading.Thread(target=producer,args=("Person",))
c1 = threading.Thread(target=consumer, args=("TD",))

p.start()
c1.start()

