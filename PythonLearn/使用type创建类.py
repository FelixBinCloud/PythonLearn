# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-09 14:16'


'''
创建类的第1方式
'''

class Foo(object):

    def func(self):
        print("Hello Foo")
print("------------------分割线------------------")
'''
创建类的第2方式
'''
def func(self):
    print("创建类的第2方式")


#构造函数
def __init__(self,name):
    self.name = name


'''
type第一个参数：类名
type第二个参数：当前类的基类,第二个参数位置是一个元组
type第三个参数：类的成员
'''
Foo = type('Foo', (object,), {'func': func, '__init__':__init__})

# 实例化
f = Foo("Td")
f.func() # 结果为：创建类的第2方式

print(type(Foo)) # <class 'type'>
'''
f对象是Foo类的一个实例，Foo类对象是 type 类的一个实例，即：Foo类对象 是通过type类的构造方法创建。

type的起源是python解释器提供的
'''