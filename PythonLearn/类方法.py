# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-09 10:19'


class Dog(object):
    name = "我是类变量"

    def __init__(self, name):
        self.name = name

    @classmethod
    def eat(self):
        print("%s is eating food" % self.name)

d = Dog("TD")
d.eat()   # 结果为：我是类变量 is eating food

'''
类方法通过@classmethod装饰器实现，类方法和普通方法的区别是， 类方法只能访问类变量，不能访问实例变量
'''