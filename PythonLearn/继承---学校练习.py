# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-04 16:44'


class School(object):
    def __init__(self, name, address):
        self.name = name
        self.address = address
        self.students = []
        self.teachers = []
        self.staffs = []

    def enroll(self, stu_obj):
        print("为学员%s办理注册手续" % stu_obj.name)
        self.students.append(stu_obj)

    def hire(self, staff_obj):
        print("雇佣了%s老师到[%s]学校工作" % (staff_obj.name, self.name))
        self.staffs.append(staff_obj)


class SchoolMember(object):  # 学校成员
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def tell(self):  # 打印方法
        pass


#  教师
class Teacher(SchoolMember):
    def __init__(self, name, age, sex, salary, course):
        super(Teacher, self).__init__(name, age, sex)
        self.salary = salary
        self.course = course

    def tell(self):  # 重构父类方法
        print('''
------------info of Teacher:%s------------
            Name:%s
            Age:%s
            Sex:%s
            Salary:%s
            Course:%s
        ''' % (self.name, self.name, self.age, self.sex, self.salary, self.course))

    def teach(self):  # 教学
        print("%s is teaching course [%s]" % (self.name, self.course))


# 学生
class Student(SchoolMember):
    def __init__(self, name, age, sex, stu_id, grade):
        super(Student, self).__init__(name, age, sex)
        self.stu_id = stu_id
        self.grade = grade

    def tell(self):
        print('''
        ------------info of Teacher:%s------------
                    Name:%s
                    Age:%s
                    Sex:%s
                    Stu_id:%s
                    Grade:%s
                ''' % (self.name, self.name, self.age, self.sex, self.stu_id, self.grade))

    # 交学费
    def pay_tuition(self, amount):
        print("%s has paid tuition for $%s" % (self.name, amount))

# 实例对象
school = School("IT学院", "阿里中心")

t1 = Teacher("WFaceBoss", 23, "男", "200000", "Python")
t2 = Teacher("FelixBin", 23, "男", "2000000", "Linux")

stu1 = Student("AL", 23, "男", 1001, "Python")
stu2 = Student("BD", "23", "男", "1002", "Linux")


# 学校雇佣两个老师
school.hire(t1)
school.hire(t2)

# 学生注册到学校
school.enroll(stu1)
school.enroll(stu2)

# 交学费
for stu in school.students:
    stu.pay_tuition(5000)

# 老师教学生
school.staffs[0].teach()






