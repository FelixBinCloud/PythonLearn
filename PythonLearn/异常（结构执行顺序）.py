# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-10 11:53'

try:
    print("try中")
except KeyError as e:
    # 异常时，执行该块
    print("异常中")
else:
    # 主代码块(try)执行完，执行该块
    print("try中正常执行后会来到else中")
finally:
    print("finally中")
    # 无论异常与否，最终执行该块
'''
(1)except KeyError as e: 键错误异常时，执行该块
   except IndexError as e:索引错误异常时，执行该块
   except Exception as e: 万能异常
(2)else:  try中正常执行后会来到else中
(3)finally:  无论异常与否，最终执行该块

'''