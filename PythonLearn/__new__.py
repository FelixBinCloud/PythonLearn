# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-09 15:08'


'''
class Foo(object):

    def __init__(self,name):
        self.name = name

        print("Foo __init__")

    def __new__(cls, *args, **kwargs):
        print("Foo __new__")
        return object.__new__(cls)
f = Foo("Td")
输出结果为：
Foo __new__
Foo __init__
'''

class Foo(object):

    def __init__(self,name):
        self.name = name

    def __new__(cls, *args, **kwargs):
        print("Foo __new__")
        #return object.__new__(cls)

f = Foo("Td")

'''
注释了return object.__new__(cls)后：
class Foo(object):

    def __init__(self,name):
        self.name = name

    def __new__(cls, *args, **kwargs):
        print("Foo __new__")
        #return object.__new__(cls)

f = Foo("Td")
结果为：Foo __new__

'''

'''
对比上述两种结果：可以得出__new__是用来实例化，在__new__中调用了__init__,简而言之，在实例化是是__new__触发了__init__方法。
return object.__new__(cls)的作用：继承父类的__new__方法
（1）object.__new__(cls)
'''