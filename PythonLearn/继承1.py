# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-04 14:56'


# 基类1
class People:
    def __init__(self, name, age, gender):
        self.name = name
        self.age = age
        self.gender = gender

    def eat(self):
        print("%s is eating..." % self.name)


# 基类2--作为基类1扩展功能的类
# 新式类
class Relation(object):
    def make_friends(self, obj):
        print("%s with %s make friends" % (self.name, obj.name))


# 子类
class Man(People, Relation):
    # 继承了People、Relation类
    def __init__(self, name, age, gender, special):
        # 先覆盖父类后继承
        super(Man, self).__init__(name, age, gender)
        self.special = special


class Woman(People):
    pass

# 实例化
m = Man("wfb", 23, "男","1")
# m.eat()
w = Woman("lgl", 22, "女")

m.make_friends(w)


# 总结
'''
 对于类的继承是会继承构造函数(对于有多个构造函数只会继承第一个)，所以在子类中重新定义构造函数是会覆盖父类的构造函数，为了还能继承父类的构造函数，需要做的是先覆盖后继承:使用super()
     def __init__(self,name,age,gender, special):
         super(Man,self).__init__(name,age,gender)
         self.special = special
'''
