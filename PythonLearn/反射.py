# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-10 11:26'

'''
反射
    hasattr(obj,name_str) , 判断一个对象obj里是否有对应的name_str字符串的方法
    getattr(obj,name_str), 根据name_str字符串去获取obj对象里的对应的方法的内存地址
    setattr(obj,'y',z), is equivalent to ``x.y = v''
    delattr    is equivalent to ``del x.y''
'''
def bulk(self):
    print("%s is yelling...." %self.name)


class Dog(object):
    def __init__(self,name):
        self.name = name

    def eat(self,food):
        print("%s is eating..."%self.name,food)


d = Dog("TD")
choice = input(">>:").strip()

if hasattr(d, choice):
    haveFunc = getattr(d, choice)
    haveFunc("food")
else:
    setattr(d, choice, bulk) #若从控制台输入的字符串为talk此时此处等价于d.talk = bulk
    Add_func = getattr(d, choice)
    Add_func(d)
