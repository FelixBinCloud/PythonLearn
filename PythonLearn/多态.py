# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-04 17:38'


class Animal(object):
    def __init__(self, name):
        self.name = name

    def talk(self):
        pass

    @staticmethod
    def animal_talk(obj):
        obj.talk()


class Dog(Animal):
    def talk(self):
        print("Woof   Woof!")


class Cat(Animal):
    def talk(self):
        print("Meow")

# 实现
d = Dog("Big Dog")
Animal.animal_talk(d)