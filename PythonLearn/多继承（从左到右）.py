# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-04 15:53'


class A:
    def __init__(self):
        print("A")


class B(A):
    def __init__(self):
        print("B")


class C(A):
    def __init__(self):
        print("C")


class D(B, C):
    pass

d = D()
# 总结
'''
新式类的写法：
class Test(object):
     pass
经典类的写法：
class Test:
     pass

（1）python2中经典类是按深度优先继承的，新式类是按广度优先继承的。
（2）python3中经典类与新式类都是统一按广度优先来继承的。




'''