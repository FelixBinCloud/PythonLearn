# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-09 10:15'


#静态方法
class Dog(object):
    def __init__(self, name):
        self.name = name

    @staticmethod  # 把eat方法变为静态方法
    def eat(self):
        print("%s is eating food" % self.name)

# 调用
d = Dog("TD")
d.eat(d)


'''
通过@staticmethod装饰器即可把其装饰的方法变为一个静态方法，什么是静态方法呢？其实不难理解，普通的方法，可以在实例化后直接调用，并且在方法里可以通过self.调用实例变量或类变量，但静态方法是不可以访问实例变量或类变量的，一个不能访问实例变量和类变量的方法，其实相当于跟类本身已经没什么关系了，它与类唯一的关联就是需要通过类名来调用这个方法

'''