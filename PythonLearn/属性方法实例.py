# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-09 10:29'


class Flight(object):
    def __init__(self, name):
        self.flight_name = name

    def checking_status(self):
        print("checking flight %s status " % self.flight_name)
        return 1

    @property
    def flight_status(self):
        status = self.checking_status()
        if status == 0:
            print("flight got canceled...")
        elif status == 1:
            print("flight is arrived...")
        elif status == 2:
            print("flight has departured already...")
        else:
            print("cannot confirm the flight status...,please check later")

    print("--------------------分割线---------------------------")
    print("--------------------修改属性值---------------------------")

    # 修改由@proerty装饰器把一个方法变成一个静态属性的值的操作-----通过@proerty.setter装饰器再装饰

    @flight_status.setter
    def flight_status(self, status):
        status_dic = {
            0: "canceled",
            1: "arrived",
            2: "departured"
        }
        print("\033[31;1mHas changed the flight status to \033[0m", status_dic.get(status))

        print("--------------------分割线---------------------------")
        print("--------------------删除属性值---------------------------")

# 删除由@proerty装饰器把一个方法变成一个静态属性的值的操作-----通过@proerty.deleter  装饰器再装饰
    @flight_status.deleter
    def flight_status(self):
        print("status got removed...")

f = Flight("CA980")
f.flight_status

f.flight_status = 1  #触发@flight_status.setter
del f.flight_status  #触发@flight_status.deleter


'''
把一个方法变成静态属性的作用：
当某个属性的值是一系列动作后才得到的结果。所以每次调用时，其实它都要经过一系列的动作才返回结果的，但这些动作过程不需要用户关心，用户只关心得到这个属性的值即可，也就是说用户只需要调用这个属性就可以了。
（1）修改属性值
  将由@proerty装饰器（把一个方法变成一个静态属性）使用@proerty.setter装饰器再装饰一下，此时 你需要写一个新方法（同名）， 对这个属性值进行更改。
（2）删除属性
 #1：将由@proerty装饰器（把一个方法变成一个静态属性）使用@proerty.deleter装饰器再装饰一下，此时 你需要写一个新方法（同名）
 #2：使用del删除该实例所对应的属性方法（属性） 例如：del f.flight_status
'''

