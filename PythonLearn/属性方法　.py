# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-09 10:24'


# 属性方法　
class Dog(object):
    def __init__(self, name):
        self.name = name

    @property
    def eat(self):
        print("%s is eating food" % self.name)

d = Dog("TD")
d.eat  # 结果为：TD is eating food

'''
属性方法的作用就是通过@property把一个方法变成一个静态属性,调用时与调用属性的方式相同

'''




