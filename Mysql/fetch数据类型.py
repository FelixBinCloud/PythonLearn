# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-25 10:20'


import pymysql

# 类似创建一个socket
conn = pymysql.connect(host='127.0.0.1',port=3306,user='root',passwd='123456',db='test')

# 游标设置为字典类型
cursor = conn.cursor(cursor=pymysql.cursors.DictCursor)

cursor.execute("select * from student")

print(cursor.fetchone())

conn.commit()
cursor.close()
conn.close()
