# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-27 9:44'

'''
多外键关联:指的是某一个表有多个字段关联了同一张表

下表中，Customer表有2个字段都关联了Address表
'''

from sqlalchemy import create_engine
from sqlalchemy import Integer, ForeignKey, String, Column
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

'''
此处连接的是本地的电脑

'''
engine = create_engine("mysql+pymysql://root:123456@localhost/Address_Customer",
                       encoding='utf-8', echo=True)

Base = declarative_base()  # 生成orm基类


class Customer(Base):
    __tablename__ = 'customer'
    id = Column(Integer, primary_key=True)
    name = Column(String(32), primary_key=False)
    billing_address_id = Column(Integer, ForeignKey("address.id"))
    shipping_address_id = Column(Integer, ForeignKey("address.id"))


    # sqlachemy就能分清哪个外键是对应哪个字段了
    billing_address = relationship("Address", foreign_keys=[billing_address_id])
    shipping_address = relationship("Address", foreign_keys=[shipping_address_id])

class Address(Base):
    __tablename__ = 'address'
    id = Column(Integer, primary_key=True)
    street = Column(String(32))
    city = Column(String(32))
    state = Column(String(32))

Base.metadata.create_all(engine)  # 创建表结构



