# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-27 10:56'


from book_author import Book,Author,engine
from sqlalchemy.orm import sessionmaker

Session_class = sessionmaker(bind=engine)
Session = Session_class()

print('--------通过书表查关联的作者---------')
book_obj = Session.query(Book).filter_by(name="Python").first()
print(book_obj.name, book_obj.authors)



print('--------通过作者表查关联的书---------')
author_obj = Session.query(Author).filter_by(name="xh").first()
print(author_obj.name, author_obj.books)


Session.commit()

