# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-27 11:10'

from book_author import Book,Author,engine
from sqlalchemy.orm import sessionmaker

Session_class = sessionmaker(bind=engine)
Session = Session_class()

'''
多对多删除
删除数据时不用管boo_m2m_authors,sqlalchemy会自动帮你把对应的数据删除
'''

# 通过书删除作者

author_obj = Session.query(Author).filter_by(name="xh").first()

book_obj = Session.query(Book).filter_by(name="Python").first()

book_obj.authors.remove(author_obj)

# 直接删除作者　

author_obj =Session.query(Author).filter_by(name="xh").first()
Session.delete(author_obj)
Session.commit()
