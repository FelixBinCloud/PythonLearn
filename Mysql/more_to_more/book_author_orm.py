# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-27 10:44'
from book_author import engine,Book,Author

from sqlalchemy.orm import sessionmaker

Session_class = sessionmaker(bind=engine)  #创建与数据库的会话session class ,注意,这里返回给session的是个class,不是实例
Session = Session_class()  # cursor

b1 = Book(name="Python")
b2 = Book(name="Linux")

a1 = Author(name="xm")
a2 = Author(name="xh")
a3 = Author(name="hh")

b1.authors = [a1, a2]
b1.authors = [a1, a2, a3]

Session.add_all([b1, b2, a1, a2, a3])
Session.commit()

