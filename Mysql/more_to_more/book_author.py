# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-27 10:34'

from sqlalchemy import create_engine,Integer,String,Table,DATE,ForeignKey,Column
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# sqlalchemy设置编码字符集一定要在数据库访问的URL上增加charset=utf8，否则数据库的连接就不是utf8的编码格式
engine = create_engine("mysql+pymysql://root:123456@localhost/school",
                       encoding='utf-8', echo=True)

Base = declarative_base()  # 生成orm基类


book_m2m_author = Table('book_m2m_author', Base.metadata,
                      Column('book_id', Integer,ForeignKey("book.id")),
                      Column('author_id', Integer,ForeignKey("author.id")),)

class Book(Base):
    __tablename__ = 'book'
    id = Column(Integer,primary_key=True)
    name = Column(String(64))
    pub_date = Column(DATE)

    authors = relationship("Author",secondary=book_m2m_author, backref='books')

    def __repr__(self):
        return self.name


class Author(Base):
    __tablename__ = 'author'
    id = Column(Integer, primary_key=True)
    name = Column(String(32))

    def __repr__(self):
        return self.name

Base.metadata.create_all(engine)  # 创建表结构