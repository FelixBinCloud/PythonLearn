# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-25 10:15'

import pymysql

conn=pymysql.connect(host='127.0.0.1',port=3306,user='root',passwd='123456',db='test')
cursor=conn.cursor()
cursor.execute("select * from student")

# 获取第一行
row_1 = cursor.fetchone()
# 获取前n行数据
row_2 = cursor.fetchmany(3)
# 获取所有数据
row_3 = cursor.fetchall()
print(row_1)
print(row_2)
print(row_3)
conn.commit()
cursor.close()
conn.close()

''''
注：在fetch数据时按照顺序进行

'''