# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-27 10:01'

from more_foreignKey import Address, Customer, engine
from sqlalchemy.orm import sessionmaker
Session_class = sessionmaker(engine)
Session = Session_class()

addr1 = Address(state="zg", city="gy", street="here")
addr2 = Address(state="zg", city="lps", street="sc")


c1 = Customer(name="xm", billing_address_id=1, shipping_address_id=2)
c2 = Customer(name="xh", billing_address_id=2, shipping_address_id=1)

# Session.add_all([addr1,addr2])
Session.add_all([c1,c2])
Session.commit()

