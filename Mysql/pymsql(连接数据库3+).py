# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-25 10:07'

import pymysql

# 创建连接
conn = pymysql.connect(host='127.0.0.1', port=3306, user='root', passwd='123456', db='test')
# 创建游标
cursor = conn.cursor()

# 执行SQL，并返回收影响行数
#effect_row = cursor.execute("update student set name = 'study' where stu_id>3")

# 执行SQL，并返回受影响行数
# effect_row = cursor.execute("update student set name = '1.1.1.2' where stu_id > %s")

# 执行SQL，并返回受影响行数   executemany :批量执行
effect_row = cursor.executemany("insert into student(name,age)values(%s,%s)", [("learn",18),("fdsfds",18)])

# 提交，不然无法保存新建或者修改的数据
conn.commit()

# 关闭游标
cursor.close()
# 关闭连接
conn.close()

# 获取最新自增ID
new_id = cursor.lastrowid
print(new_id)
