# _*_ coding:utf-8 _*_
# __author__ = 'WFaceBoss'
# __date__ = '2018-08-25 15:26'

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DATE, ForeignKey
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship

'''
此处连接的是本地的电脑

'''
engine = create_engine("mysql+pymysql://root:123456@localhost/school",
                       encoding='utf-8', echo=True)

Base = declarative_base()  # 生成orm基类


class Student(Base):
    __tablename__ = 'student'
    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)
    register_date = Column(DATE, nullable=False)

    def __repr__(self):
        return "<%s name:%s>" % (self.id, self.name)


class StudyRecord(Base):
    __tablename__ = "study_record"
    id = Column(Integer, primary_key=True)
    day = Column(Integer, nullable=False)  # nullable=False 不为空
    status = Column(String(32), nullable=False)
    stu_id = Column(Integer, ForeignKey("student.id"))  # 表名

    #student=Student()
    # student=query(Student).filter(Student.id==stu_obj.stu_id).first()
    student = relationship("Student", backref="my_classes")  # 这个nb，允许你在student表里通过backref字段反向查出所有它在StudyRecord表里的关联项


    def __repr__(self):
        return "<%s day:%s status:%s>" % (self.student.name,self.day,self.status)


Base.metadata.create_all(engine)  # 创建表结构

Session_class = sessionmaker(bind=engine)
Session = Session_class()

# student1 = Student(name="xiaohogn", register_date="2018-8-25")  # 生成你要创建的数据对象
# student2 = Student(name="xiaoli", register_date="2017-8-25")  # 生成你要创建的数据对象
# student3 = Student(name="xiaobai", register_date="2016-8-25")  # 生成你要创建的数据对象
#
# study_record1 = StudyRecord(day=1, status="YES", stu_id=1)
# study_record2 = StudyRecord(day=2, status="YES", stu_id=1)
# study_record3 = StudyRecord(day=3, status="YES", stu_id=2)
# # 加入到数据库
# Session.add_all([student1, student2, student3])
# Session.add_all([study_record1, study_record2, study_record3])
stu_obj = Session.query(Student).filter(Student.name=="xiaohogn").first()
print("stu_obj:",stu_obj)
print(stu_obj.my_classes)
Session.commit()
